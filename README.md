Shaper
======
An experiment with Pattern-matching in Node.js

Upon launch, a web interface is served that allows you to draw images in a canvas and add them to an embedded database. When adding multiple pictures, you can then let the server match your drawing with one of the previous drawings. The result will be shown in the console.
