The Algorithm
=============

##Steps
- [x] Convert the image to a two-dimensional array
- [x] Convert the array to binary (Values be either 1 or 0)
- [x] Remove the empty rows and columns of the image
- [x] Increase the size to a fixed one. This makes all images centered in a canvas

- [x] Start a for-loop that triggers for every image in the database
- [x] Start a nested for-loop for each pixel, which passes the two coordinates and images to a compare-function
- [x] This function returns a value from 0 to 100, and with which database-image it matches best
- [x] After both for-loops ended, average the scores and return an array with all database-images-names with a score

- [ ] A function should calculate a dynamic treshold to determine whether or not a match was found
- [x] Return the match, if found