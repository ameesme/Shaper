Shaper's data-model
===================
The data-model describes the way an image is written down in an object that is readable with JavaScript (and others). This is probably not the most efficient way, but it works for me.
##What it looks like
I will now display an image of 10x10 pixels, that has a plus-sign inside it with a thickness of two pixels.

`[[0],[0],[0],[0],[1],[1],[0],[0],[0],[0]],
[[0],[0],[0],[0],[1],[1],[0],[0],[0],[0]],
[[0],[0],[0],[0],[1],[1],[0],[0],[0],[0]],
[[0],[0],[0],[0],[1],[1],[0],[0],[0],[0]],
[[1],[1],[1],[1],[1],[1],[1],[1],[1],[1]],
[[1],[1],[1],[1],[1],[1],[1],[1],[1],[1]],
[[0],[0],[0],[0],[1],[1],[0],[0],[0],[0]],
[[0],[0],[0],[0],[1],[1],[0],[0],[0],[0]],
[[0],[0],[0],[0],[1],[1],[0],[0],[0],[0]],
[[0],[0],[0],[0],[1],[1],[0],[0],[0],[0]]`

##Why it looks like that
A three-dimensional-array seems a bit overkill for storing an image. Let me explain what I store in each row.
Each row represents a row of pixels in the image. The row consists out of arrays, that by default contain only one value. This value can be 0 (white) or 1 (black). The reason why I placed an array and not just an integer, is because I want to store additional data about each pixel when the algorithm is running.

###Default pixel
`[0]`

###Modified pixel
`[0,true,0.87]`

The modified pixel still contains a black/white value, but also a boolean defining that the pixel has been processed by the algorithm, and a score that is the output of the algorithm of this specific pixel.
