var express 	= require('express');
var Datastore 	= require('nedb');
var serveStatic = require('serve-static');
var bodyParser	= require('body-parser');
var fs 			= require('fs');
var app 		= express();
var jsonParser 	= bodyParser.json({limit: '20mb'});
var colors 		= require('colors');
var figlet 		= require('figlet');
var binaryDb 	= new Datastore({filename: './db/binary.db',autoload: true});
var globalVariables = {
	// Settings
	precision: 5,
	canvasSize: 200,
	cacheRefresh: 60000,

	// Not settings
	globalCallback: false,
	databaseCache: [],
	algorithmResults: [],
	algorithmTimers: []
};

app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.json());
app.use(serveStatic('../Client/'));

app.post('/shaper', jsonParser,function(req,res){
	res.end('OK!');
	console.log('===============================================================')
	console.log('ROUTER: Received Shaper-request');
	imageProcessor.dataProcessor(req.body.dataValue);
});
app.post('/insert',jsonParser,function(req,res){
	if (req.body.binaryName == '')
	{
		console.log('ERROR, input was empty');
		res.end('ERROR, input was empty');
		return;
	}
	res.end('OK!');
	console.log('===============================================================')
	console.log('ROUTER: Received database-request');
	globalVariables.globalCallback = function(){
		databaseTools.insertBinaryImage();
	};
	latestImageName = req.body.binaryName;
	imageProcessor.dataProcessor(req.body.dataValue);
})
var imageProcessor = {
	dataProcessor: function(data){
		date1 = new Date();
		console.log('PROCESSOR: Loaded canvas');
		console.log('PROCESSOR: Making canvas simple...');
		var simpleArray = Object.keys(data).map(function(key) { return data[key] });
		console.log('PROCESSOR: Simplifying colors for '+simpleArray.length/4+' pixels...');
		var size = 4;
		var smallArray = [];
		for (var i=0; i<simpleArray.length; i+=size) {
		    smallArray.push(simpleArray.slice(i,i+size));
		    if (i == simpleArray.length - 4) {
		    	imageProcessor.colorThresholder(smallArray);
		    };
		}
	},
	colorThresholder: function(smallArray){
		// Converts color values to be either 1 or 0
		binaryImage = [];
		for (var i = 0; i < smallArray.length; i++) {
			var thisRow = [];
			var thisRowInteger = 0;
			for (var j = 0; j < smallArray[i].length; j++) {
				thisRow =+smallArray[i][j];
			};
			if (thisRow > 0) {
				thisRowInteger = 1;
			};
			binaryImage.push(thisRowInteger);
		};
		imageProcessor.splitDimensions(binaryImage);
	},
	splitDimensions: function(binaryImage){
		// Converts long array to a 2-dimensional one
		var size = Math.sqrt(binaryImage.length);
		var twoDimensionalArray = []
		for (var i = 0; i < size; i++) {
			twoDimensionalArray[i] = [];
			for (var j = 0; j < size; j++) {
				twoDimensionalArray[i][j] = binaryImage[j+(i*size)];
			};
		};
		date2 = new Date();
		console.log('PROCESSOR: ['+(date2 - date1)+'ms] Array created');
		trimProcessor.StartTrimming(twoDimensionalArray);
	}
}
var trimProcessor = {
	StartTrimming: function(twoDimensionalArray){
		// Remove empty rows to leave only the drawing
		var trimmedTopBottom = trimProcessor.trimTopBottom(twoDimensionalArray);
		trimmableArray = trimProcessor.trimLeftRight(trimmedTopBottom);
		// Here, we should increase the size of the processed image again
		trimProcessor.increaseTrim();
		processedImage = trimmableArray;
		date3 = new Date();
		console.log('PROCESSOR: ['+(date3 - date2)+'ms] Finished trimming and filling array');
		// Callback to algorithm or database here
		if (globalVariables.globalCallback) {
			globalVariables.globalCallback();
		}else{
			algorithm.initializeAlgorithm();
		};
	},
	trimTopBottom: function(array){
		// Check for each array if it contains a 1, if so, add it to a new 2-dimensional array
		var tempArray = [];
		for (var i = 0; i < array.length; i++) {
			if(array[i].indexOf(1) != -1){
				tempArray.push(array[i]);
			}
		};
		date4 = new Date();
		console.log('PROCESSOR: ['+(date4 - date2)+'ms] Trimmed Top and Bottom');
		return tempArray;
	},
	trimLeftRight: function(array){
		var startTimer = new Date();
		var temparray = [];
		try{
			var neededLoops = array[0].length;
		}catch(e){
			console.log('PROCESSOR / ERROR: Could not determine size of binaryImage. Did you send an empty canvas?'.red);
			return;
		}

		for(var h = 0; h < array.length; h++) {
			temparray[h] = [];
		}
		for(var i = 0; i < neededLoops; i++) {
			var tmp = 0;
			for(var j = 0; j < array.length; j++) {
				tmp += array[j][i];
			}
			if(tmp > 0) {
				for(var k = 0; k < array.length; k++) {
					temparray[k].push(array[k][i]);
				}
			}
		}
		date5 = new Date();
		console.log('PROCESSOR: ['+(date5 - startTimer)+'ms] Trimmed Left and Right');
		return temparray;
	},
	increaseTrim: function(){
		date10 = new Date();
		var imageHeight = Math.round((globalVariables.canvasSize-trimmableArray.length)/2);
		var imageWidth = Math.round((globalVariables.canvasSize-trimmableArray[0].length)/2);
		var appendableArray = [];
		// First, create an array with the size of the imagem
		for (var i = globalVariables.canvasSize - 1; i >= 0; i--) {
			appendableArray.push(0);
		}
		// increase the width of the image
		for (var i = imageWidth - 1; i >= 0; i--) {
			for (var j = trimmableArray.length - 1; j >= 0; j--) {
				trimmableArray[j].unshift(0);
				trimmableArray[j].push(0);
			};
		};
		// Increase the height of the image
		for (var i = imageHeight - 1; i >= 0; i--) {
			trimmableArray.unshift(appendableArray);
			trimmableArray.push(appendableArray);
		};
		date11 = new Date();
		console.log('PROCESSOR: ['+(date11 - date10)+'ms] Untrimmed image with '+imageWidth+' x '+imageHeight+' pixels in width.');
	}
}
var algorithm = {
	initializeAlgorithm: function(){
		date9 = new Date();
		console.log('ALGORITHM: Starting algorithm...');
		globalVariables.algorithmTimers = [];
		// Check if there are any values in the database
		if (globalVariables.databaseCache.length > 0) {
			// Emptying previous algorithm results
			globalVariables.algorithmResults = [];
			for (var i = globalVariables.databaseCache.length - 1; i >= 0; i--) {
				var result = {
					name: globalVariables.databaseCache[i].name,
					score: algorithmTools.compareBinaries(processedImage,globalVariables.databaseCache[i])
				};
				globalVariables.algorithmResults.push(result);
			};
			date10 = new Date();
			console.log('ALGORITHM:['+(date10 - date9)+'ms] Finished algorithm.');
			console.log(('TIMER: Average processing time per image was '+util.calculateAverage(globalVariables.algorithmTimers).toFixed(2)+'ms.').yellow);
			console.log('RESULT:'.green);
			figlet(util.calculateHighest(globalVariables.algorithmResults).name, function(err, data) {
				if (err) {
					console.dir(util.calculateHighest(globalVariables.algorithmResults).name);
					return;
				}
				console.log(data.green)
			});
		}else{
			console.log('ALGORITHM / ERROR: Binary-database is empty. No data to compare with.'.red)
			return;
		};
	}
}
var algorithmTools = {
	compareBinaries: function(binary1,binary2){
		console.log('ALGORITHM-SUBTASK: Comparing \''+binary2.name+'\' with submitted image...');
		var startTimer = new Date();
		// Compares two complete binaries of the same size
		var sourceImage = binary1;
		var dbImage = binary2;
		var canvasSquare = globalVariables.canvasSize * globalVariables.canvasSize;
		var comparisonResult = 0;
		totalPixels = 0;
		for (var i = 0; i < globalVariables.canvasSize; i++) {
			for (var j = 0; j < globalVariables.canvasSize; j++) {
				comparisonResult = comparisonResult + algorithmTools.pixelSelector(binary1,binary2,[i,j]);
			};
		};
		var result = (comparisonResult/totalPixels)*100;
		var endTimer = new Date();
		globalVariables.algorithmTimers.push(endTimer - startTimer);
		console.log(('ALGORITHM-SUBTASK: ['+(endTimer - startTimer)+'ms] Finished subtask. Result is '+result.toFixed(2)+'%').green);
		return result;
	},
	detectNeighbours: function(coordinates){
		// Detects the closest neighbours of a pixel
		var topLeft = [limiter(coordinates[0]-1),limiter(coordinates[1]-1)];
		var top = [limiter(coordinates[0]),limiter(coordinates[1]-1)];
		var topRight = [limiter(coordinates[0]+1),limiter(coordinates[1]-1)];
		var right = [limiter(coordinates[0]+1),limiter(coordinates[1])];
		var bottomRight = [limiter(coordinates[0]+1),limiter(coordinates[1]+1)];
		var bottom = [limiter(coordinates[0]),limiter(coordinates[1]+1)];
		var bottomLeft = [limiter(coordinates[0]-1),limiter(coordinates[1]+1)];
		var left = [limiter(coordinates[0]-1),limiter(coordinates[1])];
		return [topLeft,top,topRight,right,bottomRight,bottom,bottomLeft,left];
	},
	pixelSelector: function(binary1,binary2,coordinates){
		// Select the same pixel from both binaries
		var pixelOne = binary1[coordinates[0]][coordinates[1]];
		var pixelTwo = binary2.processedImage[coordinates[0]][coordinates[1]];

		// Select the neighbours for each selected pixel
		var pixelNeighbours = algorithmTools.detectNeighbours(coordinates);
		pixelResults = 0;

		// Match the central-pixel
		algorithmTools.comparePixels(pixelOne,pixelTwo);

		// Match the neighbours of the central-pixel
		for (var i = 0; i < pixelNeighbours.length; i++) {
			try{
				var neighbourOne = binary1[pixelNeighbours[i][0]][pixelNeighbours[i][1]];
				var neighbourTwo = binary2.processedImage[pixelNeighbours[i][0]][pixelNeighbours[i][1]];

				algorithmTools.comparePixels(neighbourOne,neighbourTwo);
			}catch(e){
				//console.log(('ALGORITHM / ERROR: Could not read values of neighbours of pixel at '+coordinates+' in cycle '+i).red);
			}
		};
		return pixelResults;
	},
	comparePixels: function(pixelOne,pixelTwo){
		if (pixelOne == 1) {
			totalPixels++;
		};
		// Yeah...
		if (pixelOne == 1 && pixelTwo == 1) {
			pixelResults++;
		};
	}

}
var databaseTools = {
	startCache: function(){
		databaseTools.cacheBinaryDatabase();
		setInterval(function(){
			databaseTools.cacheBinaryDatabase();
		},globalVariables.cacheRefresh)
	},
	cacheBinaryDatabase: function(){
		var startTimer = new Date();
		binaryDb.find({ listingType: 'binaryImage' }, function (err, docs) {
			if (!err) {
				globalVariables.databaseCache = docs;
				date9 = new Date();
				console.log('DATABASE: ['+(date9 - startTimer)+'ms] Cached '+docs.length+' binary-images from database.');
			}else{
				date9 = new Date();
				console.log('DATABASE / ERROR: ['+(date9 - startTimer)+'ms] Could not cache from database.'.red);
				return;
			};
		});
	},
	insertBinaryImage: function(){
		var startTimer = new Date();
		var binaryImageInObject = {
			listingType: 'binaryImage',
			processedImage: processedImage,
			name: latestImageName
		};
		binaryDb.insert(binaryImageInObject, function(err, newDoc){
			if (!err) {
				var endTimer = new Date();
				console.log('DATABASE: ['+(endTimer - startTimer)+'ms] Inserted binary-image in database.');
			}else{
				var endTimer = new Date();
				console.log('DATABASE / ERROR: ['+(endTimer - startTimer)+'ms] Failed to insert binary-image in database.'.red);
				console.log(err);
				return;
			};
		});
		globalVariables.globalCallback = false;
		databaseTools.cacheBinaryDatabase();
	}
}
var util = {
	calculateHighest: function(array){
		var highestValue = 0;
		var highestScore = 0;

		for (var i = 0; i < array.length; i++) {
			if(array[i].score > highestScore){
				highestScore = array[i].score;
				highestValue = i;
			}
		};
		return array[highestValue];
	},
	calculateAverage: function(array){
		var total = 0;
		for (var i = 0; i < array.length; i++) {
			total= total + array[i];
		};
		return total/array.length;
	}
}
var limiter = function(integer){
	if (integer < 1) {
		return 0;
	}else{
		return integer;
	};
}
figlet('Shaper', function(err, data) {
	if (err) {
		console.log('Shaper')
		return;
	}
	console.log(data)
});
app.listen(8080);
databaseTools.startCache();
