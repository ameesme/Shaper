var imageProcessor = {
	process: function(){
    targetUrl = "/shaper";
		imageProcessor.readCanvasData("drawingCanvas");
	},
  insert: function(){
    targetUrl = "/insert";
    imageName = document.getElementById("imageName").value;
    imageProcessor.readCanvasData("drawingCanvas");
  },
	readCanvasData: function(canvasId){
		var canvasElement = document.getElementById(canvasId);
		var canvas2d = canvasElement.getContext("2d");
		canvasSize = [canvas2d.canvas.clientWidth,canvas2d.canvas.clientHeight];
		imageProcessor.colorExtractor(canvas2d);
	},
	colorExtractor: function(canvas2d){
		var colorData = canvas2d.getImageData(0, 0, canvasSize[0], canvasSize[1]);
		imageProcessor.dataPreparation(colorData);
	},
	dataPreparation: function(colorData){
		var colorArray = colorData.data;
		transmitter.sendToBackend('colorArray',colorArray);
	}
}
var transmitter = {
	sendToBackend: function(typeSelection,dataInformation){
    if (imageName.value !== '') {
      var transmitData = {
        dataType: typeSelection,
        dataValue: dataInformation,
        binaryName: imageName
      };
    }else{
      var transmitData = {
        dataType: typeSelection,
        dataValue: dataInformation
      };
    };
		xmlhttp=new XMLHttpRequest();
		xmlhttp.open("POST",targetUrl,true);
		xmlhttp.setRequestHeader("Content-type","application/json");
		xmlhttp.send(JSON.stringify(transmitData));
    cleanCanvas();
	}
}

window.addEventListener('load', function () {
  var canvas, context, tool;

  function init () {
    canvas = document.getElementById('drawingCanvas');
    context = canvas.getContext('2d');
    tool = new drawingTool();

    canvas.addEventListener('mousedown', ev_canvas, false);
    canvas.addEventListener('mousemove', ev_canvas, false);
    canvas.addEventListener('mouseup',   ev_canvas, false);
  }

  function drawingTool () {
    var tool = this;
    this.started = false;

    this.mousedown = function (ev) {
        context.beginPath();
        context.moveTo(ev._x, ev._y);
        tool.started = true;
    };

    this.mousemove = function (ev) {
      if (tool.started) {
        context.lineTo(ev._x, ev._y);
        context.stroke();
      }
    };

    this.mouseup = function (ev) {
      if (tool.started) {
        tool.mousemove(ev);
        tool.started = false;
      }
    };
  }

  function ev_canvas (ev) {
    if (ev.layerX || ev.layerX == 0) { // Firefox
      ev._x = ev.layerX;
      ev._y = ev.layerY;
    } else if (ev.offsetX || ev.offsetX == 0) { // Opera
      ev._x = ev.offsetX;
      ev._y = ev.offsetY;
    }

    var func = tool[ev.type];
    if (func) {
      func(ev);
    }
  }

  init();

}, false);
var cleanCanvas = function(){
  var drawingCanvas = document.getElementById('drawingCanvas');
  var imageName = document.getElementById('imageName');
  drawingCanvasContext = drawingCanvas.getContext("2d");;
  drawingCanvasContext.clearRect(0, 0, drawingCanvas.width, drawingCanvas.height);
  imageName.value = '';
}